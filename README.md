# template

#### 介绍
异步wiota测试模板，可通过静态数据自拟定测试方式

#### 软件架构
async_code 为终端固件代码

interface design为上位机界面代码


#### 使用说明

1.  通过template\interface design\dist\main.exe生成静态数据
2.  将template\async_code\bin\template_v1.0固件烧录进终端开发板中
3.  将生成的静态数据烧录进用户静态数据去进行使用

#### 注意

​	为测试版本，如有bug，敬请反馈

