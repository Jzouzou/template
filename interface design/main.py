from Ui_static_data_generation import Ui_static_data_generation
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow

class MyMainFrom(QMainWindow, Ui_static_data_generation):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.setupUi(self)
        self.pushButton.clicked.connect(self.generation)
    def generation(self):
        data = ''
        data = self.getCheckBoxValue(self.checkBox_mode.isChecked(), data)
        data = self.getCheckBoxValue(self.checkBox_uart.isChecked(), data)
        data = self.getSpinBoxValue(self.spinBox_freq.value(), data)
        data = self.getSpinBoxValue(self.spinBox_power.value(), data)
        data = self.getSpinBoxValue(self.spinBox_mcs.value(), data)
        data = self.getSpinBoxValue(self.spinBox_bc_round.value(), data)
        data = self.getSpinBoxValue(self.spinBox_fail_cnt.value(), data)
        data = self.getSpinBoxValue(self.spinBox_sub_num.value(), data)
        data = self.getCheckBoxValue(self.checkBox_is_send.isChecked(), data)
        data = self.getshorthex(self.spinBox_data_len.value(), data)
        data = self.getSpinBoxValue(self.spinBox_time.value(), data)
        data = self.getCheckBoxValue(self.checkBox_is_key.isChecked(), data)
        data = self.getSpinBoxValue(self.spinBox_key.value(), data)
        data = self.getCheckBoxValue(self.checkBox_is_bc.isChecked(), data)
        data = self.getCheckBoxValue(self.checkBox_is_light.isChecked(), data)
        self.outputWritten(data)
    
    def getCheckBoxValue(self, value, data):
        if value == False:
            data += "0x0 "
        else :
            data += "0x1 "
        return data

    def getSpinBoxValue(self, value, data):
        data += hex(value)
        data += ' '
        return data
    
    def getshorthex(self, value, data):
        data += '0x0 '
        data += hex(value & 0xff)
        data += ' '
        value = value>>8
        data += hex(value & 0xff) 
        data += ' '
        return data

    def getdata(self):
        self.outputWritten(str(self.spinBox_bc_round.value())+str(self.checkBox_is_bc.isChecked()))

    def outputWritten(self, text):
        self.textEdit.setPlainText(text)

        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWin = MyMainFrom()
    myWin.show()
    sys.exit(app.exec_())