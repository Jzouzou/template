
#ifndef _UC_WIOTA_VERSION_H__
#define _UC_WIOTA_VERSION_H__

#define GIT_TAIL_TAG   "v3.02_async"
#define GIT_UPDATE_VERSION_TIME  "Wed Jan 10 11:26:40 2024"
#define TIMESTAMP   (__DATE__ " " __TIME__)

#endif

