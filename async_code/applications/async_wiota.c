#include "async_wiota.h"

#ifdef USING_AT_UART
at_server_t get_at_uart();
rt_size_t at_vprintf(rt_device_t device, const char* format, va_list args);
#endif

#ifdef USING_RT_UART
rt_device_t get_rt_uart();
#endif


typedef struct 
{
    unsigned char freq;
    unsigned char power;
    unsigned char mcs;
    unsigned char bc_round;
    unsigned char fail_cnt;
    unsigned char sub_num;
}wiota_config_t;

typedef struct 
{
    unsigned char is_send;
    unsigned short data_len;
    unsigned char time; //s
    unsigned char is_key;
    unsigned char key_pin;
    unsigned char is_bc;
    unsigned char light;
}terminal_config_t;

typedef struct  
{
    unsigned char mode;
    unsigned char uart;
    wiota_config_t wiota_cfg;
    terminal_config_t terminal_cfg;
}user_static, * user_static_p;


user_static_p g_user_static = RT_NULL;
rt_sem_t wiota_sem = RT_NULL;
rt_sem_t key_sem = RT_NULL;
unsigned char* send_message = RT_NULL;
unsigned char* recv_message = RT_NULL;
unsigned short recv_len = 0;
unsigned int my_userid = 0xaabbcc00;
unsigned int target_userid = 0xaabbcc11;


void user_uart_send(const char* fmt, ...)
{

    if (0 == g_user_static->uart)
    {
        return ;
    }

    va_list args;
#ifdef USING_AT_UART
    at_server_t at_server_local = RT_NULL;
    at_server_local = get_at_uart();
    va_start(args, fmt);
    at_vprintf(at_server_local->device, fmt, args);
    va_end(args);
#endif

#ifdef USING_RT_UART
    rt_device_t _console_device = RT_NULL;
    _console_device = get_rt_uart();
    rt_size_t length;
    static char rt_log_buf[RT_CONSOLEBUF_SIZE];

    va_start(args, fmt);

    length = rt_vsnprintf(rt_log_buf, sizeof(rt_log_buf) - 1, fmt, args);
    if (length > RT_CONSOLEBUF_SIZE - 1)
    { length = RT_CONSOLEBUF_SIZE - 1; }
    if (_console_device != RT_NULL)
    {
        rt_uint16_t old_flag = _console_device->open_flag;

        _console_device->open_flag |= RT_DEVICE_FLAG_STREAM;
        rt_device_write(_console_device, 0, rt_log_buf, length);
        _console_device->open_flag = old_flag;
    }
    va_end(args);
#endif

}


int user_static_init()
{
    user_static_p static_data = NULL;
    unsigned char * address = NULL;
    address = uc_wiota_get_user_info();

    int flag = 0; 
    for (int i = 0; i < sizeof(user_static); i++)
    {
        if (address[i] != 0)
        {
            flag = 1;
            break;
        }
    }
    if (flag == 0)
    {
        at_server_printfln("static data error");
        return RT_ERROR;
    }


    static_data = (user_static_p )address;

    g_user_static = static_data;

    user_uart_send("mode = %d uart = %d\n",static_data->mode, static_data->uart);

    user_uart_send("bc round = %d fail cnt = %d freq = %d mcs = %d power = %d sub num = %d\n", 
                    static_data->wiota_cfg.bc_round,
                    static_data->wiota_cfg.fail_cnt, 
                    static_data->wiota_cfg.freq, 
                    static_data->wiota_cfg.mcs, 
                    static_data->wiota_cfg.power,
                    static_data->wiota_cfg.sub_num);

    user_uart_send("data len = %d bc = %d key = %d send = %d pin = %d time = %d\n", 
                    static_data->terminal_cfg.data_len, 
                    static_data->terminal_cfg.is_bc, 
                    static_data->terminal_cfg.is_key, 
                    static_data->terminal_cfg.is_send, 
                    static_data->terminal_cfg.key_pin, 
                    static_data->terminal_cfg.time);

    return RT_EOK;
}

void wiota_recv(uc_recv_back_p data)
{
    at_server_printf("+WIOTARECV:");
    recv_message = data->data;
    recv_len = data->data_len;
    rt_sem_release(wiota_sem);
}

void send_message_create()
{
    unsigned char *send_data_temp = RT_NULL;
    send_data_temp = rt_malloc(g_user_static->terminal_cfg.data_len + 2);
    for (int i = 0; i < g_user_static->terminal_cfg.data_len; i++)
    {
        send_data_temp[i] = rand() % 26 + 'a';
    }

    send_message = send_data_temp;

}

void wiota_send()
{
    send_message_create();
    at_server_printf("+WIOTASEND:");
    at_send_data(send_message, g_user_static->terminal_cfg.data_len);
    uc_wiota_send_data(target_userid, (unsigned char *)send_message, g_user_static->terminal_cfg.data_len, RT_NULL, 0, 5000, RT_NULL);
    
    rt_free(send_message);
}

void wiota_init(void)
{
    sub_system_config_t config;

    uc_wiota_init();

    uc_wiota_get_system_config(&config);
    at_server_printfln("symbol length = %d, bandwidth = %d, spectrum_idx = %d, subsystemid = %08x",config.symbol_length, config.bandwidth, config.spectrum_idx, config.subsystemid);

    uc_wiota_set_freq_info(g_user_static->wiota_cfg.freq);
    at_server_printfln("freq = %d", uc_wiota_get_freq_info());

    if (g_user_static->terminal_cfg.is_send == 0)
    {
        my_userid = my_userid ^ target_userid;
        target_userid = my_userid ^ target_userid;
        my_userid = my_userid ^ target_userid;
    }

    if (1 == g_user_static->terminal_cfg.is_bc)
    {
        target_userid = 0;
    }

    uc_wiota_set_userid(&my_userid, 4);
    at_server_printfln("userid = %04x", my_userid);
    user_uart_send("target userid = %04x", target_userid);

    uc_wiota_set_bc_round(g_user_static->wiota_cfg.bc_round);
    uc_wiota_set_subframe_num(g_user_static->wiota_cfg.sub_num);
    uc_wiota_set_unisend_fail_cnt(g_user_static->wiota_cfg.fail_cnt);
    uc_wiota_set_max_power(22);
    uc_wiota_set_cur_power(g_user_static->wiota_cfg.power);
    
    uc_wiota_set_data_rate(UC_RATE_NORMAL, g_user_static->wiota_cfg.mcs);


    uc_wiota_run();
    uc_wiota_register_recv_data_callback(wiota_recv, UC_CALLBACK_NORAMAL_MSG);

    uc_wiota_light_func_enable(g_user_static->terminal_cfg.light);

}


void wiota_recv_task(void *para)
{

    at_server_printf("wiota recv task begin\n");
    while (1)
    {
        rt_sem_take(wiota_sem, RT_WAITING_FOREVER);
        at_send_data(recv_message, recv_len);

        if (1 == g_user_static->mode)
        {
            wiota_send();
        }

        rt_free(recv_message);
    }
    
}

int wiota_recv_thread_init()
{
    wiota_sem = rt_sem_create("wiota_sem", 0, RT_IPC_FLAG_PRIO);
    if(wiota_sem == RT_NULL)
    {
        user_uart_send("wiota_sem creat fail");
        return RT_ERROR;
    }
    rt_thread_t wiota_recv_thread_handle = RT_NULL;
    wiota_recv_thread_handle = rt_thread_create("wiota_recv_thread", wiota_recv_task, RT_NULL, 512, RT_THREAD_PRIORITY_MAX / 3, 5);
    if (wiota_recv_thread_handle == RT_NULL)
    {
        user_uart_send("wiota thread creat fail");
        return RT_ERROR;
    }
    rt_thread_startup(wiota_recv_thread_handle);
    return RT_EOK;
}


void wiota_send_task(void *para)
{

    at_server_printf("wiota send task begin\n");
    
    

    while (1)
    {
        if (0 == g_user_static->terminal_cfg.is_key && 1 == g_user_static->terminal_cfg.is_send)
        {
            rt_thread_mdelay(g_user_static->terminal_cfg.time * 1000);
        }
        else
        {
            rt_sem_take(key_sem, RT_WAITING_FOREVER);
        }
        wiota_send();
    }
    
}

int wiota_send_thread_init()
{
    rt_thread_t wiota_send_thread_handle = RT_NULL;
    wiota_send_thread_handle = rt_thread_create("wiota_send_thread", wiota_send_task, RT_NULL, 512, RT_THREAD_PRIORITY_MAX / 3, 5);
    if (wiota_send_thread_handle == RT_NULL)
    {
        user_uart_send("wiota send thread creat fail");
        return RT_ERROR;
    }
    rt_thread_startup(wiota_send_thread_handle);
    return RT_EOK;
}


void key_task()
{
    user_uart_send("key task begin\n");
    // rt_pin_write(g_user_static->terminal_cfg.key_pin, PIN_HIGH);
    rt_thread_mdelay(100);
    rt_pin_mode(g_user_static->terminal_cfg.key_pin, PIN_MODE_INPUT);
    while (1)
    {
        if (rt_pin_read(g_user_static->terminal_cfg.key_pin) == PIN_HIGH)
        {
            rt_thread_mdelay(10);
            if (rt_pin_read(g_user_static->terminal_cfg.key_pin) == PIN_HIGH)
            {
                rt_sem_release(key_sem);
                while (rt_pin_read(g_user_static->terminal_cfg.key_pin) == PIN_HIGH)
                {

                }
            }
        }
        // rt_thread_mdelay(500);
    }
}

int key_thread_init()
{
    user_uart_send("key thread init\n");
    key_sem = rt_sem_create("key_sem", 0, RT_IPC_FLAG_PRIO);
    if(key_sem == RT_NULL)
    {
        user_uart_send("key_sem creat fail");
        return RT_ERROR;
    }
    rt_thread_t key_thead_handle;
    key_thead_handle = rt_thread_create("key_thread", key_task, RT_NULL, 512, RT_THREAD_PRIORITY_MAX / 3, 5);
    if (key_thead_handle == RT_NULL)
    {
        user_uart_send("key thread creat fail");
        return RT_ERROR;
    }
    rt_thread_startup(key_thead_handle);
    return RT_EOK;
}

int wiota_thread_init(void)
{
    if (RT_EOK != user_static_init())
    {
        return RT_ERROR;
    }
    wiota_init();
    if (1 == g_user_static->terminal_cfg.is_key)
    {
        if(RT_EOK != key_thread_init())
        {
            return RT_ERROR;
        }
    }
    if (RT_EOK != wiota_recv_thread_init())
    {    
        return RT_ERROR;
    }
    if (RT_EOK != wiota_send_thread_init())
    {
        return RT_ERROR;
    }
        

    return RT_EOK;
}