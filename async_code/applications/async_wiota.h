#ifndef __ASYNC_WIOTA_H__
#define __ASYNC_WIOTA_H__

#include <rtthread.h>
#include <rtdevice.h>
#include "at.h"
#include "uc_wiota_api.h"
#include "uc_wiota_static.h"
#include "stdio.h"
#include "stdlib.h"

#define USING_AT_UART
#define USING_RT_UART      

#define WIOTA_FREQ          (45)
#define WIOTA_BC_ROUND      (1)
#define WIOTA_FAIL_CNT      (2)
#define WIOTA_CUR_POWER     (0)
#define WIOTA_SUB_NUM       (7)

int wiota_thread_init(void);

#endif